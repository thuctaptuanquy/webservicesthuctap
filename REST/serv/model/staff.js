import mongoose, { Schema } from 'mongoose';
const StaffSchema = new Schema({
    name:{
        type: String,
        required: true
    },
    age: {
        type: Number,
        required: true
    },
    address: String,
},
{
    timestamps: true
});

export default mongoose.model('Staff', StaffSchema);