var express = require('express');
var router = express.Router();
import * as controller from '../controller/staff';

/* GET home page. */
router.get('/', function(req, res, next) {
  controller.list().then(data=>{
    res.json({message:data});
  })
  .catch(err=>{
    res.json({err : err.message});
  });
});

router.post('/', (req, res, next)=>{
  controller.create(req.body).then(data=>{
    res.json({message: data});
  }).catch(err=>{
    res.json({err : err.message});
  });
});

router.put('/', (req, res, next)=>{
  console.log(req.body);  
  controller.edit(req.body._id, req.body).then(data=>{
    res.json({message: data});
  }).catch(err=>{
    res.json({err: err.message});
  });
});

router.delete('/', (req,res, next)=>{
  controller.del(req.body).then(data=>{
    res.json({message: data});
  }).catch(err=>{
    res.json({err: err.message});
  });
})
module.exports = router;
