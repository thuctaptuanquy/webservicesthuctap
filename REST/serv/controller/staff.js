import Staff from '../model/staff';

export function list(){
    return new Promise((resolve, reject)=>{
        Staff.find().exec()
        .then(data=>{
            if(data==null){
                reject('chua co du lieu');
            }
            else{
                resolve(data);
            }
        })
        .catch(err=>{
            reject(err);
        })
    })
}
export function create(body){
    let newStaff = new Staff({
        name: body.name,
        age: body.age,
        address: body.address,
    });
    return new Promise((resolve, reject)=>{
        newStaff.save()
        .then(()=>resolve('Post thanh cong'))
        .catch((err)=>reject(err));
    });
}

export function del(body){
    return new Promise((resolve, reject)=>{
        Staff.findByIdAndRemove(body._id, (err, data)=>{
            if (err) {
                reject(err)
            } else {
                if(data==null){
                    reject('khong thay ban ghi');
                }else{
                    resolve('da xoa');
                }                
            }
        })
    })
}

export function edit(_id,body){
    return new Promise((resolve, reject)=>{
        Staff.findByIdAndUpdate(_id, {name: body.name, age: body.age, address: body.address}, {new: true}, (err, data)=>{
            if (err) {
                reject(err)
            } else {
                if(data==null){
                    reject('khong thay ban ghi');
                }else{
                    resolve(data);
                }                
            }
        })
    })
}