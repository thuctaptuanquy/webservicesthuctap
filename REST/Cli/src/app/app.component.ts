import { Component } from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Staff} from './app.model';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  // name: string = '';

  staffs: any[];
  constructor(private httpClient: HttpClient) {}
  // onNameKeyUp(event: any) {
  //  this.name = event.target.value;
  // }
  onSelectedStaff() {}
  getProfile() {
    this.httpClient.get('http://localhost:3000/')
    .subscribe(
      (data) => {
        this.staffs = data['message'];
      }
    );
  }
  sendProfile() {
    this.httpClient.post('http://localhost:3000/', {name: 'jun321', age: '20', address: 'qo'})
    .subscribe(res => {
      console.log(res);
    }, err => {
      console.log(err);
    });
  }
}
